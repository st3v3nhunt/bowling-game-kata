require './bowling'

describe Bowling do

	before(:each) do
		@bowling = Bowling.new
	end

	describe 'score' do
		it 'should return 0 for an all gutter game' do
			roll_many(20, 0)
			@bowling.score.should == 0
		end
		
		it 'should return 20 for an all 1 game' do
			roll_many(20, 1)
			@bowling.score.should == 20
		end
		
		it 'should return 16 for a spare followed by 3' do
			roll_spare
			@bowling.roll(3)
			roll_many(17, 0)
			@bowling.score.should == 16
		end
		
		it 'should return 24 for strike followed by 3 then 4' do
			roll_strike
			@bowling.roll(3)
			@bowling.roll(4)
			roll_many(16, 0)
			@bowling.score.should == 24
		end
		
		it 'should return 300 for a perfect game' do
			roll_many(12, 10)
			@bowling.score.should == 300
		end
		
	end
end

def roll_many(rolls, pins)
	rolls.times { @bowling.roll(pins) }
end

def roll_spare
	@bowling.roll(5)
	@bowling.roll(5)
end

def roll_strike
	@bowling.roll(10)
end